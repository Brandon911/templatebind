static get properties() {
    return {
        prop1: {type: String},
        prop2: {type: String},
        prop3: {type: Boolean},
        prop4: {type: String}
    };
}
